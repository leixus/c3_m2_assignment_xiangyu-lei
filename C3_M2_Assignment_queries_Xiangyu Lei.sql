/* Provided schema and test data */
-- Create Tables
CREATE TABLE warehouses (
    warehouse_id SERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    location VARCHAR(100) NOT NULL
);

CREATE TABLE carriers (
    carrier_id SERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    contact_person VARCHAR(50),
    contact_number VARCHAR(15)
);

CREATE TABLE shipments (
    shipment_id SERIAL PRIMARY KEY,
    tracking_number VARCHAR(20) UNIQUE NOT NULL,
    weight DECIMAL(10, 2) NOT NULL,
    status VARCHAR(20) DEFAULT 'Pending',
    warehouse_id INT REFERENCES warehouses(warehouse_id),
    carrier_id INT REFERENCES carriers(carrier_id)
);

-- Seed Data
INSERT INTO warehouses (name, location) VALUES
    ('Warehouse A', 'Location A'),
    ('Warehouse B', 'Location B');

INSERT INTO carriers (name, contact_person, contact_number) VALUES
    ('Carrier X', 'John Doe', '123-456-7890'),
    ('Carrier Y', 'Jane Smith', '987-654-3210');

INSERT INTO shipments (tracking_number, weight, warehouse_id, carrier_id) VALUES
    ('ABC123', 150.5, 1, 1),
    ('XYZ789', 200.0, 2, 2);


/* Tasks */
-- 1.	Views
-- (a)	Create a view named "warehouse_shipments" that displays the tracking number, 
--		weight, and status of shipments along with the warehouse name for each shipment.
CREATE VIEW warehouse_shipments AS
SELECT tracking_number, weight, status, name AS warehouse_name
FROM shipments AS s
INNER JOIN warehouses AS w ON s.warehouse_id = w.warehouse_id;

SELECT * FROM warehouse_shipments;

-- (b)	Create a view named "carrier_shipments" that shows the tracking number, weight, 
--		and status of shipments along with the carrier name for each shipment.
CREATE VIEW carrier_shipments AS
SELECT tracking_number, weight, status, name AS carrier_name
FROM shipments AS s
INNER JOIN carriers AS ca ON s.carrier_id = ca.carrier_id;

SELECT * FROM carrier_shipments;

-- 2.	Common Table Expressions (CTEs)
-- (a)	Create a CTE named "pending_shipments" that includes the tracking number, weight, 
--		and warehouse location for all shipments with the status 'Pending'.
WITH pending_shipments AS (
	SELECT tracking_number, weight, location AS warehouse_location
	FROM shipments AS s
	INNER JOIN warehouses AS w ON s.warehouse_id = w.warehouse_id
	WHERE LOWER(status) = 'pending'
)
SELECT * FROM pending_shipments;

-- (b) Create a CTE named "heavy_shipments" that includes the tracking number, weight, and
--		carrier name for all shipments with a weight greater than 200.
WITH heavy_shipments AS (
	SELECT tracking_number, weight, name AS carrier_name
	FROM shipments AS s
	INNER JOIN carriers AS ca ON s.carrier_id = ca.carrier_id
	WHERE weight >= 200
)
SELECT * FROM heavy_shipments;

-- 3.	Transactions
-- (a)	Write a transaction that updates the status of the shipment with tracking 
--		number 'ABC123' to 'Shipped' and increments the weight by 10 units.
BEGIN;
	UPDATE shipments SET status = 'Shipped', weight = weight+10 WHERE tracking_number = 'ABC123';
COMMIT;

SELECT * FROM shipments;

-- (b)	Write another transaction that inserts a new shipment with tracking number 
--		'LMN456', weight 180.75, into Warehouse B using Carrier Y.
BEGIN;
	INSERT INTO shipments(tracking_number, weight, warehouse_id, carrier_id)
	VALUES('LMN456', 180.75, 2, 2);
COMMIT;

SELECT * FROM shipments;

-- 4.	Indexes
--		Create an appropriate index on "tracking_number" in table "shipments" to enhance the
--		performance of below query.

--		Before creating index on "tracking_number":
--		Planning Time: ~ 0.104 ms
--		Execution Time: ~ 0.046 ms

--		After creating index on "tracking_number":
--		Planning Time: ~ 0.452 ms
--		Execution Time: ~ 0.033 ms

--		The performance is not improved so much regarding the time, because the database is too small
--		to reveal the improvement.

CREATE INDEX idx_tracking_number ON shipments USING HASH(tracking_number);

EXPLAIN ANALYZE SELECT tracking_number, weight, status
FROM shipments
WHERE tracking_number = 'XYZ789';
